package viewmodel;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import model.MobTimer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.time.Duration;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(JUnitParamsRunner.class)
public class StopWatchViewModelTest {

	private MobTimer _mobTimer;
	private StopWatchViewModel _stopWatchViewModel;

	@Before
	public void setup() {
		_mobTimer = Mockito.mock(MobTimer.class);
		_stopWatchViewModel = new StopWatchViewModel(_mobTimer);
	}

	@SuppressWarnings("unused")
	private static Iterable getSecondParameters() {
		return Arrays.asList(new Object[][]{
				{Duration.ZERO, "00"},
				{Duration.ofMinutes(1), "00"},
				{Duration.ofMinutes(2), "00"},
				{Duration.ofSeconds(30), "30"},
				{Duration.ofSeconds(5), "05"}
		});
	}

	@Test
	@Parameters(method = "getSecondParameters")
	public void getSecondsLeft_returnsCorrectlyFormatted(Duration givenDuration,
														 String expectedSeconds) {
		doReturn(givenDuration)
				.when(_mobTimer)
				.getTimeLeft();

		assertEquals(expectedSeconds,
					 _stopWatchViewModel.getSecondsLeft());
	}

	@SuppressWarnings("unused")
	private static Iterable getMinutesParameters() {
		return Arrays.asList(new Object[][]{
				{Duration.ZERO, "00"},
				{Duration.ofMinutes(1), "01"},
				{Duration.ofSeconds(30), "00"},
				{Duration.ofMinutes(50), "50"},
				{Duration.ofMinutes(150), "150"}
		});
	}

	@Test
	@Parameters(method = "getMinutesParameters")
	public void getMinutesLeft_returnsCorrectlyFormatted(Duration givenDuration,
														 String expectedMinutes) {
		doReturn(givenDuration)
				.when(_mobTimer)
				.getTimeLeft();

		assertEquals(expectedMinutes,
					 _stopWatchViewModel.getMinutesLeft());
	}

	@Test
	public void getRunState_startsAsPlay() {
		assertEquals(StopWatchViewModel.PLAY,
					 _stopWatchViewModel.getRunStateText());
	}

	@Test
	public void toggleRunState_fromStart_changesToPause() {
		doReturn(true)
				.when(_mobTimer)
				.isPlaying();
		_stopWatchViewModel.toggleRunState();
		verify(_mobTimer)
				.pause();
	}

	@Test
	public void toggleRunState_fromPause_changesToStart() {
		doReturn(false)
				.when(_mobTimer)
				.isPlaying();
		_stopWatchViewModel.toggleRunState();
		verify(_mobTimer)
				.start();

	}

	@Test
	public void getRunStateText_isPlaying_showPause() {
		doReturn(true)
				.when(_mobTimer)
				.isPlaying();
		assertEquals(StopWatchViewModel.PAUSE,
					 _stopWatchViewModel.getRunStateText());
	}

	@Test
	public void getRunStateText_isPaused_showsPlay() {
		doReturn(false)
				.when(_mobTimer)
				.isPlaying();
		assertEquals(StopWatchViewModel.PLAY,
					 _stopWatchViewModel.getRunStateText());
	}

	@Test
	public void getTimerEditable_isPaused_true() {
		doReturn(false)
				.when(_mobTimer)
				.isPlaying();
		assertTrue(_stopWatchViewModel.getTimerEditable());

	}

	@Test
	public void getTimerEditable_isPlaying_false() {
		doReturn(true)
				.when(_mobTimer)
				.isPlaying();
		assertFalse(_stopWatchViewModel.getTimerEditable());

	}

	@Test
	public void setMinutes_noChangeToSeconds() {
		doReturn(Duration.ofMinutes(10).plusSeconds(15))
				.when(_mobTimer)
				.getTimeLeft();

		_stopWatchViewModel.setMinutesLeft("05");
		verify(_mobTimer)
				.setDuration(Duration.ofMinutes(5).plusSeconds(15));

	}

	@Test
	public void setSeconds_noChangeToMinutes() {
		doReturn(Duration.ofMinutes(10).plusSeconds(15))
				.when(_mobTimer)
				.getTimeLeft();

		_stopWatchViewModel.setSecondsLeft("05");
		verify(_mobTimer)
				.setDuration(Duration.ofMinutes(10).plusSeconds(5));

	}
}
