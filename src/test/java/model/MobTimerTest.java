package model;

import com.google.common.collect.ImmutableList;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import static com.intellij.testFramework.UsefulTestCase.assertEmpty;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(JUnitParamsRunner.class)
public class MobTimerTest {

	private Clock _clock;
	private MobTimer _subject;

	@Before
	public void setup() {
		_clock = mock(Clock.class);
		setCurrentTime(Instant.EPOCH);
		_subject = new MobTimer(_clock,
								mobTimer -> {
								});
	}

	private void setCurrentTime(Instant currentTime) {
		doReturn(currentTime)
				.when(_clock)
				.instant();
	}

	@Test
	public void getTimeLeft_timerNotStarted_durationIsSet() {
		MobTimer subject = new MobTimer(_clock,
										mobTimer -> {
										});
		subject.setDuration(Duration.ofMinutes(10));

		assertEquals(Duration.ofMinutes(10),
					 subject.getTimeLeft());
	}

	@Test
	public void getTimeLeft_timerStarted_durationChanges() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		moveCurrentTimeByOneMinute();

		assertEquals(Duration.ofMinutes(9),
					 _subject.getTimeLeft());
	}

	@Test
	public void getTimeLeft_timerPastDue_returnsZeroDuration() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		moveCurrentTimeBySeconds(60 * 11);

		assertEquals(Duration.ZERO,
					 _subject.getTimeLeft());
	}

	@Test
	public void restart_afterTimePassed_getTimeLeftReturnsOriginalTime() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		moveCurrentTimeBySeconds(60 * 11);
		_subject.restart();

		assertEquals(Duration.ofMinutes(10),
					 _subject.getTimeLeft());
	}

	@Test
	public void reset_afterTimePassedAndPaused_getTimeLeftReturnsOriginalTime() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		moveCurrentTimeByOneMinute();
		_subject.pause();
		_subject.restart();

		assertEquals(Duration.ofMinutes(10),
					 _subject.getTimeLeft());
	}

	@Test
	public void reset_afterTimePassedPausedUnpaused_getTimeLeftReturnsOriginalTime() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		moveCurrentTimeByOneMinute();
		_subject.pause();
		_subject.start();
		moveCurrentTimeByOneMinute();
		_subject.restart();

		assertEquals(Duration.ofMinutes(10),
					 _subject.getTimeLeft());
	}

	@Test
	public void reset_selectsNextMobberInList() {
		_subject.addMobber("first");
		_subject.addMobber("second");

		_subject.restart();

		assertEquals(1,
					 _subject.getCurrentMobber());
	}

	@Test
	public void reset_lastMobberSelected_selectsFirstMobberInList() {
		_subject.addMobber("first");
		_subject.addMobber("second");
		_subject.setCurrentMobber(1);

		_subject.restart();

		assertEquals(0,
					 _subject.getCurrentMobber());

	}

	@Test
	public void getTimeLeft_neverSet_returnsZero() {
		assertEquals(Duration.ZERO,
					 _subject.getTimeLeft());
	}

	@Test
	public void pause_timeStaysAtPauseTime() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		moveCurrentTimeByOneMinute();
		assertEquals(Duration.ofMinutes(9),
					 _subject.getTimeLeft());
		_subject.pause();
		moveCurrentTimeByOneMinute();

		assertEquals(Duration.ofMinutes(9),
					 _subject.getTimeLeft());
	}

	@Test
	public void start_afterPause_timeContinuesAsNormal() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		_subject.pause();
		moveCurrentTimeByOneMinute();
		_subject.start();
		moveCurrentTimeByOneMinute();

		assertEquals(Duration.ofMinutes(9),
					 _subject.getTimeLeft());
	}

	@Test
	public void setDuration_toDurationLeft_resetStillSetsToOriginalDuration() {
		_subject.setDuration(Duration.ofMinutes(10));
		_subject.start();
		moveCurrentTimeByOneMinute();
		_subject.setDuration(Duration.ofMinutes(9).plusMillis(1));

		_subject.restart();
		assertEquals(Duration.ofMinutes(10),
					 _subject.getTimeLeft());


	}

	@SuppressWarnings("unused")
	private static Iterable getTimesUpScenarios() {
		return Arrays.asList(new Object[][]{
				{Duration.ZERO, false},
				{Duration.ofSeconds(30), false},
				{Duration.ofMinutes(1), true},
				{Duration.ofMinutes(2), true}
		});
	}

	@Test
	@Parameters(method = "getTimesUpScenarios")
	public void isTimeUp_withOneMinuteDurationAndTimePasses_correctTimesUp(Duration timeToPass,
																		   boolean isTimeUp) {
		_subject.setDuration(Duration.ofMinutes(1));
		_subject.start();
		moveCurrentTimeBySeconds(timeToPass.getSeconds());
		assertEquals(isTimeUp,
					 _subject.isTimeUp());
	}

	@Test
	public void isTimeUp_durationZero_isTrue() {
		_subject.setDuration(Duration.ZERO);
		_subject.start();
		assertTrue(_subject.isTimeUp());
	}

	@Test
	public void isTimeUp_durationReset_false() {
		_subject.setDuration(Duration.ofMinutes(1));
		_subject.start();
		moveCurrentTimeByOneMinute();
		_subject.restart();

		assertFalse(_subject.isTimeUp());
	}

	@Test
	public void isTimeUp_durationZeroAndIsPaused_isFalse() {
		_subject.setDuration(Duration.ZERO);
		assertFalse(_subject.isTimeUp());
	}

	@Test
	public void isPlaying_noStartedYet_false() {
		assertFalse(_subject.isPlaying());
	}

	@Test
	public void isPlaying_started_true() {
		_subject.start();
		assertTrue(_subject.isPlaying());
	}

	@Test
	@Parameters(value = {"-1", "1"})
	public void removeMobber_mobberDoesntExist_nothingHappens(int index) {
		_subject.addMobber("fred");
		_subject.removeMobber(index);
		assertEquals(ImmutableList.of("fred"),
					 _subject.getMobbers());
	}

	@Test
	public void setMobbers_givenValidAndInvalidMobbers_onlyAddsValidMobbers() {
		_subject.setMobbers(new String[]{"validMobber", " "});
		assertEquals(ImmutableList.of("validMobber"),
					 _subject.getMobbers());
	}

	@Test
	public void addMobber_firstMobberAdded_setAsCurrentMobber() {
		_subject.addMobber("fred");
		_subject.addMobber("fred2");
		assertEquals("fred",
					 _subject.getMobbers().get(_subject.getCurrentMobber()));
	}

	@Test
	@Parameters(value = {"", " ", "\t", "\n"})
	public void addMobber_whitespaceGiven_nothingAdded(String whiteSpace) {
		_subject.addMobber(whiteSpace);
		assertEmpty(_subject.getMobbers());
	}

	@Test
	public void removeMobber_nextInListIsSelected() {
		_subject.addMobber("fred");
		_subject.addMobber("fred2");
		_subject.addMobber("fred3");
		_subject.setCurrentMobber(1);
		_subject.removeMobber(1);
		assertEquals("fred3",
					 _subject.getMobbers().get(_subject.getCurrentMobber()));
	}

	@Test
	public void removeMobber_lastMobberInList_PreviousInListIsSelected() {
		_subject.addMobber("fred");
		_subject.addMobber("sally");
		_subject.setCurrentMobber(1);
		_subject.removeMobber(1);
		assertEquals("fred",
					 _subject.getMobbers().get(_subject.getCurrentMobber()));
	}

	@Test
	public void moveMobber_pastEndOfList_putsAtEnd() {
		_subject.addMobber("fred");
		_subject.addMobber("harriot");
		_subject.addMobber("sally");
		_subject.moveMobber(0,
							1000);
		assertEquals(ImmutableList.of("harriot",
									  "sally",
									  "fred"),
					 _subject.getMobbers());
	}

	@Test
	public void moveMobber_beforeStartOfList_putsAtStart() {
		_subject.addMobber("fred");
		_subject.addMobber("philip");
		_subject.addMobber("sally");
		_subject.moveMobber(_subject.getMobbers().size() - 1,
							-1000);
		assertEquals(ImmutableList.of("sally",
									  "fred",
									  "philip"),
					 _subject.getMobbers());

	}

	@Test
	public void moveMobber_moveOneDown_movesOneDown() {
		_subject.addMobber("fred");
		_subject.addMobber("philip");
		_subject.addMobber("sally");
		_subject.moveMobber(0,
							1);
		assertEquals(ImmutableList.of("philip",
									  "fred",
									  "sally"),
					 _subject.getMobbers());

	}

	@Test
	public void moveMobber_moveOneUp_movesOneUp() {
		_subject.addMobber("fred");
		_subject.addMobber("philip");
		_subject.addMobber("sally");
		_subject.moveMobber(1,
							0);
		assertEquals(ImmutableList.of("philip",
									  "fred",
									  "sally"),
					 _subject.getMobbers());

	}

	private void moveCurrentTimeByOneMinute() {
		moveCurrentTimeBySeconds(60);
	}

	private void moveCurrentTimeBySeconds(long secondsToAdd) {
		setCurrentTime(Instant.now(_clock)
							  .plusSeconds(secondsToAdd));
	}
}