package model;

import com.google.common.collect.ImmutableList;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class MobTimer {
	private static final Function<Clock, Instant> CURRENT_START_TIME_FACTORY = Instant::now;
	private final Clock _clock;
	private final Consumer<MobTimer> _repository;
	private final List<String> _mobbers = new ArrayList<>();
	private Duration _lastPausedDuration = Duration.ZERO;
	private Duration _totalDuration = Duration.ZERO;
	private Function<Clock, Instant> _startTimeFactory;
	private int _currentMobber;

	public MobTimer(Clock clock,
					Consumer<MobTimer> repository) {
		_clock = clock;
		_repository = repository;
		startTimeIsCurrentTime();
	}

	private void startTimeIsCurrentTime() {
		_startTimeFactory = MobTimer.CURRENT_START_TIME_FACTORY;
	}


	public void setDuration(Duration duration) {
		if (getTimeLeft().getSeconds() == duration.getSeconds()) {
			return;
		}
		_totalDuration = duration;
		_lastPausedDuration = _totalDuration;
		_repository.accept(this);
	}

	public Duration getTimeLeft() {
		Duration durationLeft = _lastPausedDuration.minus(Duration.between(_startTimeFactory.apply(_clock),
																		   Instant.now(_clock)));
		if (durationLeft.isNegative()) {
			return Duration.ZERO;
		}
		return durationLeft;
	}

	public void start() {
		Instant whenThisMethodWasLastCalled = Instant.now(_clock);
		_startTimeFactory = (ignored) -> whenThisMethodWasLastCalled;
	}

	public void pause() {
		_lastPausedDuration = getTimeLeft();
		startTimeIsCurrentTime();
	}

	public void restart() {
		start();
		_lastPausedDuration = _totalDuration;
		int nextMobber = getCurrentMobber() + 1;
		if (nextMobber == getMobbers().size()) {
			nextMobber = 0;
		}
		setCurrentMobber(nextMobber);
	}

	public int getCurrentMobber() {
		return _currentMobber;
	}

	public boolean isTimeUp() {
		return getTimeLeft().isZero() && isPlaying();
	}

	public boolean isPlaying() {
		return _startTimeFactory != MobTimer.CURRENT_START_TIME_FACTORY;
	}

	public void addMobber(String mobber) {
		if (mobber.trim().isEmpty()) {
			return;
		}
		_mobbers.add(mobber.trim());
		_repository.accept(this);
	}

	public ImmutableList<String> getMobbers() {
		return ImmutableList.copyOf(_mobbers);
	}

	public void removeMobber(int selectedIndex) {
		if (selectedIndex < 0 || _mobbers.size() <= selectedIndex) {
			return;
		}
		_mobbers.remove(selectedIndex);
		if (_mobbers.size() <= getCurrentMobber()) {
			setCurrentMobber(_mobbers.size() - 1);
		}
		_repository.accept(this);
	}

	public void setCurrentMobber(int currentMobber) {
		_currentMobber = keepIndexInBounds(currentMobber);
	}

	public void setMobbers(String[] mobbers) {
		_mobbers.clear();
		if (mobbers == null) {
			return;
		}
		Arrays.stream(mobbers)
			  .forEach(this::addMobber);
	}

	public Duration getFullDuration() {
		return _totalDuration;
	}

	public void moveMobber(int fromIndex,
						   int toIndex) {
		toIndex = keepIndexInBounds(toIndex);

		String movingMobber = _mobbers.remove(fromIndex);
		_mobbers.add(toIndex,
					 movingMobber);
		setCurrentMobber(toIndex);

	}

	private int keepIndexInBounds(int toIndex) {
		return Math.min(_mobbers.size() - 1,
						Math.max(0,
								 toIndex));
	}

	public void moveUpCurrentMobber() {

		int toIndex = getCurrentMobber() - 1;
		moveMobber(getCurrentMobber(),
				   toIndex);
	}

	public void moveDownCurrentMobber() {
		int toIndex = getCurrentMobber() + 1;
		moveMobber(getCurrentMobber(),
				   toIndex);
	}
}
