package viewmodel.infrastructure;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.ArrayList;
import java.util.List;

public class ChangeNotifier {

	private boolean _isNotifyingListeners = false;
	private final List<ChangeListener> _changeListeners = new ArrayList<>();

	public void onPropertyChanged() {

		if (_isNotifyingListeners) {
			return;
		}
		_isNotifyingListeners = true;
		_changeListeners.forEach(listener -> listener.stateChanged(new ChangeEvent(this)));
		_isNotifyingListeners = false;
	}

	public void addListener(ChangeListener changeListener) {
		_changeListeners.add(changeListener);
	}
}
