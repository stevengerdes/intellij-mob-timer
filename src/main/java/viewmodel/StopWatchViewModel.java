package viewmodel;

import com.google.common.collect.ImmutableList;
import model.MobTimer;
import viewmodel.infrastructure.ChangeNotifier;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.time.Duration;

public class StopWatchViewModel {

	static final String PLAY = "/mobTimer/play.png";
	static final String PAUSE = "/mobTimer/pause.png";
	private final MobTimer _mobTimer;
	private final Timer _timer;
	private final ChangeNotifier _onChange = new ChangeNotifier();

	public StopWatchViewModel(MobTimer mobTimer) {
		_mobTimer = mobTimer;
		_timer = new Timer(100,
						   e -> _onChange.onPropertyChanged());
	}

	public void addPropertyChangedListener(ChangeListener changeListener) {
		_onChange.addListener(changeListener);
	}

	public String getSecondsLeft() {
		return String.format("%02d",
							 getSeconds());
	}

	private long getSeconds() {
		return _mobTimer.getTimeLeft().minusMinutes(getMinutes())
						.getSeconds();
	}

	public String getMinutesLeft() {
		return String.format("%02d",
							 getMinutes());

	}

	private long getMinutes() {
		return _mobTimer.getTimeLeft().toMinutes();
	}

	public boolean isTimeUp() {
		return _mobTimer.isTimeUp();
	}

	public boolean getTimerEditable() {
		return !_mobTimer.isPlaying();
	}

	public String getRunStateText() {
		if (_mobTimer.isPlaying()) {
			return StopWatchViewModel.PAUSE;
		} else {
			return StopWatchViewModel.PLAY;
		}
	}

	public void toggleRunState() {
		if (_mobTimer.isPlaying()) {
			_mobTimer.pause();
		} else {
			_mobTimer.start();
		}
		updateTimerBasedOnRunState();
		_onChange.onPropertyChanged();

	}

	private void updateTimerBasedOnRunState() {
		if (_mobTimer.isPlaying()) {
			_timer.start();
		} else {
			_timer.stop();
		}
	}

	public void setSecondsLeft(String seconds) {
		_mobTimer.setDuration(Duration.ofMinutes(getMinutes()).plusSeconds(Long.parseLong(seconds)));
		_onChange.onPropertyChanged();
	}

	public void setMinutesLeft(String minutes) {
		_mobTimer.setDuration(Duration.ofSeconds(getSeconds()).plusMinutes(Long.parseLong(minutes)));
		_onChange.onPropertyChanged();
	}

	public void reset() {
		_mobTimer.restart();
		updateTimerBasedOnRunState();
		_onChange.onPropertyChanged();
	}

	public void moveMobber(String mobberName) {
		_mobTimer.addMobber(mobberName);
		_onChange.onPropertyChanged();
	}

	public ImmutableList<String> getMobbers() {
		return _mobTimer.getMobbers();
	}

	public void removeCurrentMobber() {
		_mobTimer.removeMobber(getCurrentMobber());
		_onChange.onPropertyChanged();
	}

	public void setCurrentMobber(int currentMobber) {
		_mobTimer.setCurrentMobber(currentMobber);
		_onChange.onPropertyChanged();
	}

	public int getCurrentMobber() {
		return _mobTimer.getCurrentMobber();
	}

	public void moveUpCurrentMobber() {
		_mobTimer.moveUpCurrentMobber();
		_onChange.onPropertyChanged();
	}

	public void moveDownCurrentMobber() {
		_mobTimer.moveDownCurrentMobber();
		_onChange.onPropertyChanged();
	}

	public void moveMobber(int fromIndex,
						   int toIndex) {
		_mobTimer.moveMobber(fromIndex,
							 toIndex);
		_onChange.onPropertyChanged();

	}
}
