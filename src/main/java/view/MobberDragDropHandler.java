package view;

import viewmodel.StopWatchViewModel;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DragSource;
import java.util.Objects;

class MobberDragDropHandler extends TransferHandler {
	private final DataFlavor localObjectFlavor;
	private final StopWatchViewModel _stopWatchViewModel;
	private int _movedFromIndex = -1;

	public MobberDragDropHandler(StopWatchViewModel stopWatchViewModel) {
		super();
		_stopWatchViewModel = stopWatchViewModel;
		localObjectFlavor = new DataFlavor(Object[].class,
										   "Array of items");
	}

	@Override
	protected Transferable createTransferable(JComponent c) {
		JList<?> source = (JList<?>) c;
		c.getRootPane().getGlassPane().setVisible(true);

		_movedFromIndex = source.getSelectedIndices()[0];
		Object[] transferedObjects = source.getSelectedValuesList().toArray(new Object[0]);
		return new Transferable() {
			@Override
			public DataFlavor[] getTransferDataFlavors() {
				return new DataFlavor[]{localObjectFlavor};
			}

			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return Objects.equals(localObjectFlavor,
									  flavor);
			}

			@Override
			public Object getTransferData(DataFlavor flavor)
					throws UnsupportedFlavorException {
				if (isDataFlavorSupported(flavor)) {
					return transferedObjects;
				} else {
					throw new UnsupportedFlavorException(flavor);
				}
			}
		};
	}

	@Override
	public boolean canImport(TransferSupport info) {
		return info.isDrop() && info.isDataFlavorSupported(localObjectFlavor);
	}

	@Override
	public int getSourceActions(JComponent c) {
		c.getRootPane()
		 .getGlassPane()
		 .setCursor(DragSource.DefaultMoveDrop);
		return TransferHandler.MOVE;
	}

	@Override
	public boolean importData(TransferSupport info) {
		TransferHandler.DropLocation tdl = info.getDropLocation();
		if (!canImport(info) || !(tdl instanceof JList.DropLocation)) {
			return false;
		}

		int toIndex = ((JList.DropLocation) tdl).getIndex();
		_stopWatchViewModel.moveMobber(_movedFromIndex,
									   _movedFromIndex < toIndex ? toIndex - 1 : toIndex);
		return true;
	}

	@Override
	protected void exportDone(JComponent c,
							  Transferable data,
							  int action) {
		c.getRootPane().getGlassPane().setVisible(false);
		_movedFromIndex = -1;
	}

}
