package view;

import com.google.common.base.Strings;
import com.intellij.openapi.roots.ui.componentsList.layout.Orientation;
import org.apache.commons.lang3.StringUtils;
import view.infrastructure.StretchTextField;
import viewmodel.StopWatchViewModel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class TimerView extends JPanel implements ChangeListener {

	private final StopWatchViewModel _stopWatchViewModel;
	private final Supplier<Orientation> _orientationSupplier;

	private final StretchTextField _minuteLabel;
	private final StretchTextField _secondLabel;

	TimerView(StopWatchViewModel stopWatchViewModel,
			  Supplier<Orientation> orientationSupplier) {
		_stopWatchViewModel = stopWatchViewModel;
		_stopWatchViewModel.addPropertyChangedListener(this);
		_orientationSupplier = orientationSupplier;

		setLayout(new BoxLayout(this,
								BoxLayout.X_AXIS));
		_minuteLabel = addResizingLabel("00");
		initTimeSegment(_minuteLabel,
						StopWatchViewModel::setMinutesLeft);
		addResizingLabel(":").setEditable(false);
		_secondLabel = addResizingLabel("00");
		initTimeSegment(_secondLabel,
						StopWatchViewModel::setSecondsLeft);

		add(Box.createGlue());
		stateChanged(null);
	}

	private void initTimeSegment(final StretchTextField segmentTextField,
								 final BiConsumer<StopWatchViewModel, String> segmentValueGetter) {
		segmentTextField.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {

			}

			@Override
			public void focusLost(FocusEvent e) {
				String text = segmentTextField.getText();
				segmentValueGetter.accept(_stopWatchViewModel,
										  text);
			}
		});
		DefaultCaret c = new DefaultCaret();
		c.setBlinkRate(segmentTextField.getCaret().getBlinkRate());
		segmentTextField.setCaret(c);
		segmentTextField.setHighlighter(null);
		segmentTextField.setDocument(new TwoDigitTextFieldDocumentFilter());
	}

	private StretchTextField addResizingLabel(String text) {
		StretchTextField resizeLabelFont = new StretchTextField(text,
																_orientationSupplier);
		JPanel resizingPanel = new JPanel() {
			@Override
			public Dimension getMaximumSize() {
				Dimension preferredSize = super.getMaximumSize();
				preferredSize.width = resizeLabelFont.getSize().width;
				return preferredSize;
			}
		};
		resizingPanel.setLayout(new BorderLayout());
		resizingPanel.add(resizeLabelFont,
						  BorderLayout.CENTER);
		add(resizingPanel);
		return resizeLabelFont;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		_minuteLabel.setText(_stopWatchViewModel.getMinutesLeft());
		_minuteLabel.setEditable(_stopWatchViewModel.getTimerEditable());
		_secondLabel.setText(_stopWatchViewModel.getSecondsLeft());
		_secondLabel.setEditable(_stopWatchViewModel.getTimerEditable());
	}

	private class TwoDigitTextFieldDocumentFilter extends PlainDocument {
		@Override
		public void insertString(int offset,
								 String typedString,
								 AttributeSet attr) throws BadLocationException {
			if (typedString == null || !StringUtils.isNumeric(typedString)) {
				return;
			}

			if (getLength() == 0) {
				super.insertString(offset,
								   typedString,
								   attr);
				return;
			}

			if (typedString.length() == 1) {
				String existingSting = getContent().getString(0,
															  getLength());

				String lastCharacter = existingSting.isEmpty() ? "" : existingSting.substring(existingSting.length() - 1);
				String newString = lastCharacter + typedString;
				newString = Strings.padStart(newString,
											 2,
											 '0');
				replace(0,
						2,
						newString,
						attr);
			} else {
				replace(0,
						2,
						typedString,
						attr);
			}

		}
	}
}
