package view;

import com.google.common.collect.ImmutableMap;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ui.componentsList.layout.Orientation;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowAnchor;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import model.MobTimer;
import org.jetbrains.annotations.NotNull;
import viewmodel.StopWatchViewModel;

import java.time.Clock;
import java.time.Duration;
import java.util.function.Supplier;

public class MobTimerFactory implements ToolWindowFactory, DumbAware {

	public static final String MOBTIMER_MOBBERS = "mobtimer.mobbers";
	public static final String MOBTIMER_FULL_DURATION = "mobtimer.fullDuration";

	@Override
	public void createToolWindowContent(@NotNull Project project,
										@NotNull ToolWindow toolWindow) {
		MobTimerView mobTimerView;
		MobTimer mobTimer = new MobTimer(Clock.systemDefaultZone(),
										 mobTimerToSave -> {
											 PropertiesComponent savedState = PropertiesComponent.getInstance();
											 savedState.setValue(MobTimerFactory.MOBTIMER_FULL_DURATION,
																 String.valueOf(mobTimerToSave.getFullDuration()
																							  .getSeconds()));
											 savedState.setValues(MobTimerFactory.MOBTIMER_MOBBERS,
																  mobTimerToSave.getMobbers().toArray(new String[0]));
										 });

		PropertiesComponent savedState = PropertiesComponent.getInstance();
		Duration fullDuration = Duration.ofSeconds(savedState.getOrInitLong(MobTimerFactory.MOBTIMER_FULL_DURATION,
																			0));
		String[] mobberList = savedState.getValues(MobTimerFactory.MOBTIMER_MOBBERS);
		mobTimer.setDuration(fullDuration);
		mobTimer.setMobbers(mobberList);
		StopWatchViewModel stopWatchViewModel = new StopWatchViewModel(mobTimer);

		mobTimerView = new MobTimerView(stopWatchViewModel,
										createOrientationSupplier(toolWindow));

		stopWatchViewModel.addPropertyChangedListener(e -> {
			if (stopWatchViewModel.isTimeUp()) {
				toolWindow.show(() -> {
				});
				mobTimerView.requestFocus();
			}
		});
		toolWindow.setTitle("Mob Timer");
		ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
		Content content = contentFactory.createContent(mobTimerView,
													   "",
													   false);
		toolWindow.getContentManager().addContent(content);
	}

	@NotNull
	private Supplier<Orientation> createOrientationSupplier(ToolWindow toolWindow) {
		ImmutableMap<ToolWindowAnchor, Orientation> anchorToOrientation = ImmutableMap.of(ToolWindowAnchor.BOTTOM,
																						  Orientation.HORIZONTAL,
																						  ToolWindowAnchor.TOP,
																						  Orientation.HORIZONTAL,
																						  ToolWindowAnchor.LEFT,
																						  Orientation.VERTICAL,
																						  ToolWindowAnchor.RIGHT,
																						  Orientation.VERTICAL);

		return () -> anchorToOrientation.get(toolWindow.getAnchor());
	}
}
