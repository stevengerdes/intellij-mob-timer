package view;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.intellij.ui.components.JBList;
import com.intellij.ui.components.JBScrollPane;
import viewmodel.StopWatchViewModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class MobbersView extends JPanel {

	private boolean _isSelectingFromViewModel;
	private final StopWatchViewModel _stopWatchViewModel;
	private final DefaultListModel<String> _dataModel;
	private final JBList<String> _mobberListComponent;

	public MobbersView(StopWatchViewModel stopWatchViewModel) {
		_stopWatchViewModel = stopWatchViewModel;
		setLayout(new BorderLayout());
		JPanel componentHolder = new JPanel();
		componentHolder.setLayout(new BoxLayout(componentHolder,
												BoxLayout.Y_AXIS));
		add(componentHolder,
			BorderLayout.CENTER);

		_dataModel = new DefaultListModel<>();
		_mobberListComponent = new JBList<>(_dataModel);
		_mobberListComponent.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		_mobberListComponent.setDragEnabled(true);
		_mobberListComponent.setDropMode(DropMode.INSERT);
		_mobberListComponent.setTransferHandler(new MobberDragDropHandler(stopWatchViewModel));
		_mobberListComponent.setSelectedIndex(0);
		_mobberListComponent.setVisibleRowCount(5);
		_mobberListComponent.addListSelectionListener(e -> {
			if (e.getValueIsAdjusting() || _isSelectingFromViewModel) {
				return;
			}
			int currentMobber = stopWatchViewModel.getCurrentMobber();
			if (currentMobber == e.getFirstIndex()) {
				stopWatchViewModel.setCurrentMobber(e.getLastIndex());
			} else {
				stopWatchViewModel.setCurrentMobber(e.getFirstIndex());
			}
		});

		_mobberListComponent.addKeyListener(new KeyListener() {

			Set<Integer> _activeKeys = new HashSet<>();

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				_activeKeys.add(e.getKeyCode());
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					stopWatchViewModel.removeCurrentMobber();
				}

				if (_activeKeys.containsAll(Sets.newHashSet(KeyEvent.VK_SHIFT,
															KeyEvent.VK_ALT,
															KeyEvent.VK_UP))) {
					stopWatchViewModel.moveUpCurrentMobber();
				}

				if (_activeKeys.containsAll(Sets.newHashSet(KeyEvent.VK_SHIFT,
															KeyEvent.VK_ALT,
															KeyEvent.VK_DOWN))) {
					stopWatchViewModel.moveDownCurrentMobber();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				_activeKeys.remove(e.getKeyCode());
			}
		});
		JScrollPane listScrollPane = new JBScrollPane(_mobberListComponent);
		componentHolder.add(listScrollPane);


		JPanel addMobberPanel = new JPanel();
		addMobberPanel.setLayout(new BoxLayout(addMobberPanel,
											   BoxLayout.X_AXIS));
		JButton nameAdderButton = new JButton("Add");
		addMobberPanel.add(nameAdderButton);
		JTextField nameAdderField = new JTextField();
		Dimension buttonHeightWithGrowingWidth = nameAdderButton.getMaximumSize();
		buttonHeightWithGrowingWidth.setSize(Integer.MAX_VALUE,
											 buttonHeightWithGrowingWidth.getHeight());
		nameAdderField.setMaximumSize(buttonHeightWithGrowingWidth);
		addMobberPanel.add(nameAdderField);

		componentHolder.add(listScrollPane);
		componentHolder.add(addMobberPanel);

		nameAdderField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					addNewMobber(nameAdderField);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		});
		stopWatchViewModel.addPropertyChangedListener(e -> {
			updateMobbersView();
		});
		nameAdderButton.addActionListener(e -> {
			addNewMobber(nameAdderField);
		});

		updateMobbersView();
	}

	private void updateMobbersView() {
		_isSelectingFromViewModel = true;
		_dataModel.removeAllElements();
		ImmutableList<String> mobbers = _stopWatchViewModel.getMobbers();
		for (String mobber : mobbers) {
			_dataModel.addElement(mobber);
		}
		_mobberListComponent.setSelectedIndex(_stopWatchViewModel.getCurrentMobber());
		_mobberListComponent.ensureIndexIsVisible(_stopWatchViewModel.getCurrentMobber());
		_isSelectingFromViewModel = false;
	}

	private void addNewMobber(JTextField nameAdderField) {
		_stopWatchViewModel.moveMobber(nameAdderField.getText());
		nameAdderField.setText("");
	}
}
