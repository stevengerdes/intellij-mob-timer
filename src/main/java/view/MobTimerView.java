package view;

import com.intellij.openapi.roots.ui.componentsList.layout.Orientation;
import com.intellij.util.ui.JBUI;
import org.jetbrains.annotations.NotNull;
import view.infrastructure.StretchIcon;
import view.infrastructure.StretchTextButton;
import viewmodel.StopWatchViewModel;

import javax.swing.*;
import java.awt.*;
import java.util.function.Supplier;

class MobTimerView extends JPanel {

	MobTimerView(StopWatchViewModel stopWatchViewModel,
				 Supplier<Orientation> orientationSupplier) {
		setLayout(new BoxLayout(this,
								BoxLayout.X_AXIS));

		add(new TimerView(stopWatchViewModel,
						  orientationSupplier));
		add(Box.createHorizontalStrut(10));
		addResizingComponent(getPlayButton(stopWatchViewModel));
		add(Box.createHorizontalStrut(10));
		addResizingComponent(getResetButton(stopWatchViewModel,
											orientationSupplier));

		add(new MobbersView(stopWatchViewModel));
	}

	@NotNull
	private JButton getResetButton(StopWatchViewModel stopWatchViewModel,
								   Supplier<Orientation> orientationSupplier) {
		JButton reset = new StretchTextButton("Next",
											  orientationSupplier);

		reset.addActionListener(e -> stopWatchViewModel.reset());
		return reset;
	}

	private JButton getPlayButton(StopWatchViewModel stopWatchViewModel) {
		JButton playPause = new JButton();
		playPause.setIcon(new StretchIcon(getClass().getResource(stopWatchViewModel.getRunStateText())));
		playPause.addActionListener(e -> stopWatchViewModel.toggleRunState());
		stopWatchViewModel.addPropertyChangedListener(e -> playPause.setIcon(new StretchIcon(getClass().getResource(stopWatchViewModel
																															.getRunStateText()))));
		return playPause;
	}

	private void addResizingComponent(Component button) {
		JPanel resizingPanel = new JPanel();
		resizingPanel.setLayout(new BorderLayout());
		resizingPanel.setBorder(JBUI.Borders.empty(10));
		resizingPanel.add(button,
						  BorderLayout.CENTER);

		add(resizingPanel);
	}

	/**
	 * @(#)StretchIcon.java 1.0 03/27/12
	 */


}
