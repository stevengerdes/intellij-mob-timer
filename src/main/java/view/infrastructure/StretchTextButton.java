package view.infrastructure;

import com.intellij.openapi.roots.ui.componentsList.layout.Orientation;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.function.Function;
import java.util.function.Supplier;

public class StretchTextButton extends JButton {
	private static final int MIN_FONT_SIZE = 3;
	private static final int MAX_FONT_SIZE = 240;
	private Graphics _graphics;
	private final Supplier<Orientation> _orientationSupplier;


	public StretchTextButton(String text,
							 Supplier<Orientation> orientationSupplier) {
		super(text);
		_orientationSupplier = orientationSupplier;
		init();
	}

	private void init() {
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				adaptLabelFont();
			}
		});
	}

	private void adaptLabelFont() {
		if (_graphics == null) {
			return;
		}

		setFont(createResizedFont(getFontSizeThatFillsOrientationSize(getBounds().getSize())));
		repaint();
	}

	private Font createResizedFont(int fontSize) {
		return getFont().deriveFont(getFont().getStyle(),
									fontSize);
	}

	@NotNull
	private Function<Dimension, Double> getOrientedSizeGetter() {
		if (_orientationSupplier.get() == Orientation.HORIZONTAL) {
			return Dimension::getHeight;
		}
		if (_orientationSupplier.get() == Orientation.VERTICAL) {
			return Dimension::getWidth;
		}
		throw new RuntimeException("unexpected orientation");

	}

	private int getFontSizeThatFillsOrientationSize(Dimension desiredDimensions) {
		int fontSize = StretchTextButton.MIN_FONT_SIZE;
		Function<Dimension, Double> orientedSizeGetter = getOrientedSizeGetter();

		while (fontSize < StretchTextButton.MAX_FONT_SIZE) {
			Dimension dimensionOfFontSize = getDimensionForFont(createResizedFont(fontSize));
			Dimension dimensionForNextFont = getDimensionForFont(createResizedFont(fontSize + 1));
			if (between(orientedSizeGetter.apply(desiredDimensions),
						orientedSizeGetter.apply(dimensionOfFontSize),
						orientedSizeGetter.apply(dimensionForNextFont))) {
				break;
			}
			fontSize++;
		}
		return fontSize;
	}

	private boolean between(double orientationSize,
							double minSize,
							double maxSize) {
		return orientationSize >= minSize
				&& orientationSize <= maxSize;
	}

	private Dimension getDimensionForFont(Font font) {
		Dimension size = new Dimension();
		_graphics.setFont(font);
		FontMetrics fm = _graphics.getFontMetrics(font);
		size.width = fm.stringWidth(getText());
		size.height = fm.getHeight();

		return size;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		_graphics = g;
	}

}
